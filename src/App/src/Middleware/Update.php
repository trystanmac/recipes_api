<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Middleware\Exception\BadRequest;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\InputFilter\InputFilterInterface;

class Update implements MiddlewareInterface
{
    /** @var InputFilterInterface $inputFilter */
    private $inputFilter;

    /** @var EntityManager $entityManager */
    private $entityManager;

    /** @var array $inputMethodMapper */
    private $inputMethodMapper;

    /**
     * Update constructor.
     * @param InputFilterInterface $inputFilter
     * @param EntityManager $entityManager
     * @param array $inputMethodMapper
     */
    public function __construct(
        InputFilterInterface $inputFilter,
        EntityManager $entityManager,
        array $inputMethodMapper
    ) {
        $this->inputFilter = $inputFilter;
        $this->entityManager = $entityManager;
        $this->inputMethodMapper = $inputMethodMapper;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws BadRequest
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $entity = $request->getAttribute('Data');

        /**
         * @todo have a check to ensure that we have
         *  an entity here.
         */

        // Check if we are creating or updating
        $httpStatusCode = StatusCodeInterface::STATUS_CREATED;

        if (null !== $entity->getId()) {
            $httpStatusCode = StatusCodeInterface::STATUS_OK;
        }

        /** @var array $parsedBody */
        $parsedBody = $request->getParsedBody();

        $this->inputFilter->setData($parsedBody);

        if (!$this->inputFilter->isValid()) {
            throw new BadRequest(json_encode($this->inputFilter->getMessages()));
        }

        /** @var array $valuesFromFilter */
        $valuesFromFilter = $this->inputFilter->getValues();

        foreach ($this->inputMethodMapper as $input => $method) {
            $entity->$method($valuesFromFilter[$input]);
        }

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        /** @var ServerRequestInterface $requestWithAttribute */
        $requestWithAttribute = $request->withAttribute('Data', $entity);

        return $handler
            ->handle($requestWithAttribute)
            ->withStatus($httpStatusCode);
    }
}
