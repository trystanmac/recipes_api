<?php

declare(strict_types=1);

namespace App\Middleware\Recipe;

use App\Entity\Recipe as RecipeEntity;
use App\Middleware\Exception\NotFound as NotFoundException;
use Doctrine\ORM\EntityRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class GetItem
 * @package App\Middleware\Recipe
 */
class GetItem implements MiddlewareInterface
{
    /** @var EntityRepository $entityRepository */
    private $entityRepository;

    /**
     * Recipe constructor.
     * @param EntityRepository $entityRepository
     */
    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws NotFoundException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /** @var integer $recipeId */
        $recipeId = $request->getAttribute('id');

        /** @var RecipeEntity $recipeEntity */
        $recipeEntity = $this->entityRepository->find($recipeId);

        if (!$recipeEntity instanceof RecipeEntity) {
            throw new NotFoundException();
        }

        /** @var ServerRequestInterface $requestWithAttribute */
        $requestWithAttribute = $request->withAttribute('Data', $recipeEntity);

        return $handler->handle($requestWithAttribute);
    }
}
