<?php

declare(strict_types=1);

namespace App\Middleware\Recipe;

use App\Middleware\Exception\BadRequest as BadRequestException;
use Doctrine\ORM\EntityRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * Class GetCollection
 * @package App\Middleware\Recipe
 */
class GetCollection implements MiddlewareInterface
{
    /** @var EntityRepository $entityRepository */
    private $entityRepository;

    /** @var InputFilterInterface $inputFilter */
    private $inputFilter;

    /**
     * GetCollection constructor.
     * @param EntityRepository $entityRepository
     * @param InputFilterInterface $inputFilter
     */
    public function __construct(
        EntityRepository $entityRepository,
        InputFilterInterface $inputFilter
    ) {
        $this->entityRepository = $entityRepository;
        $this->inputFilter = $inputFilter;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws BadRequestException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $queryParams = $request->getQueryParams();

        $this->inputFilter->setData($queryParams);

        if (!$this->inputFilter->isValid()) {
            $inputFilterMessages = $this->inputFilter->getMessages();
            throw new BadRequestException(json_encode($inputFilterMessages));
        }

        $filteredParams = [];

        /**
         * We only want to pass non null attribute values to the
         * findBy method.
         */
        foreach ($this->inputFilter->getValues() as $attribute => $value) {
            if (null === $value) {
                continue;
            }
            $filteredParams[$attribute] = $value;
        }

        $entities = $this->entityRepository
            ->findBy(
                $filteredParams,
                ['id' => 'ASC'],
                $queryParams['num'],
                $queryParams['from']
            );

        /** @var ServerRequestInterface $requestWithAttribute */
        $requestWithAttribute = $request->withAttribute('Data', $entities);

        return $handler->handle($requestWithAttribute);
    }
}
