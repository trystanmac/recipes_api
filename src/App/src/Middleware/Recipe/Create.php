<?php

declare(strict_types=1);

namespace App\Middleware\Recipe;

use App\Entity\Recipe as RecipeEntity;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class Create
 * @package App\Middleware\Recipe
 */
class Create implements MiddlewareInterface
{
    /** @var RecipeEntity $recipeEntity */
    private $recipeEntity;

    /**
     * Create constructor.
     * @param RecipeEntity $recipeEntity
     */
    public function __construct(RecipeEntity $recipeEntity)
    {
        $this->recipeEntity = $recipeEntity;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $requestWithAttribute = $request->withAttribute('Data', $this->recipeEntity);

        return $handler->handle($requestWithAttribute);
    }
}
