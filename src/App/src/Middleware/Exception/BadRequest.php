<?php


namespace App\Middleware\Exception;

use Exception;
use Throwable;

/**
 * Class BadException
 * @package App\Middleware\Exception
 */
class BadRequest extends Exception
{
    public function __construct($message = "Not found", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
