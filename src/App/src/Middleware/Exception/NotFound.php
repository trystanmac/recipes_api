<?php


namespace App\Middleware\Exception;

use Exception;
use Throwable;

/**
 * Class NotFound
 * @package App\Middleware\Exception
 */
class NotFound extends Exception
{
    public function __construct($message = "Not found", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
