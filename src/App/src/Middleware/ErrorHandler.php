<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Middleware\Exception\BadRequest as BadRequestException;
use App\Middleware\Exception\NotFound as NotFoundException;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class ErrorHandler
 * @package App\Middleware
 */
class ErrorHandler implements MiddlewareInterface
{
    /** @var JsonResponse $jsonResponse */
    private $jsonResponse;

    /** @var EmptyResponse $emptyResponse */
    private $emptyResponse;

    /**
     * ErrorHandler constructor.
     * @param JsonResponse $jsonResponse
     * @param EmptyResponse $emptyResponse
     */
    public function __construct(
        JsonResponse $jsonResponse,
        EmptyResponse $emptyResponse
    ) {
        $this->jsonResponse = $jsonResponse;
        $this->emptyResponse = $emptyResponse;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            return $handler->handle($request);
        } catch (Throwable $throwable) {

            /**
             * @todo Some logging would be useful here.
             */
            if ($throwable instanceof NotFoundException) {
                return $this
                    ->emptyResponse
                    ->withStatus(StatusCodeInterface::STATUS_NOT_FOUND);
            }

            $httpCode = $this->getHttpCode($throwable);
            return $this
                ->jsonResponse
                ->withPayload(['error' => $throwable->getMessage()])
                ->withStatus($httpCode);
        }
    }

    /**
     * @param Throwable $throwable
     * @return int
     */
    private function getHttpCode(Throwable $throwable): int
    {
        if ($throwable instanceof NotFoundException) {
            return StatusCodeInterface::STATUS_NOT_FOUND;
        }

        if ($throwable instanceof BadRequestException) {
            return StatusCodeInterface::STATUS_BAD_REQUEST;
        }

        return StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR;
    }
}
