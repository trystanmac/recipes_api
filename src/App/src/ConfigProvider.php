<?php

declare(strict_types=1);

namespace App;

use Doctrine\ORM\EntityManager;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies() : array
    {
        return [
            'factories'  => [
                EntityManager::class                => Factory\EntityManager::class,
                Handler\NotFound::class             => Factory\Handler\NotFound::class,
                Handler\Resource::class             => Factory\Handler\Resource::class,
                'Handler\ResourceItem'              => Factory\Handler\ResourceItem::class,
                'Handler\ResourceCollection'        => Factory\Handler\ResourceCollection::class,
                'Handler\Recipe\ResourceItem'       => Factory\Handler\Recipe\ResourceItem::class,
                'Handler\Recipe\ResourceCollection' => Factory\Handler\Recipe\ResourceCollection::class,

                Middleware\ErrorHandler::class      => Factory\Middleware\ErrorHandler::class,
                Middleware\Recipe\Create::class     => Factory\Middleware\Recipe\Create::class,
                Middleware\Recipe\GetCollection::class => Factory\Middleware\Recipe\GetCollection::class,
                Middleware\Recipe\GetItem::class    => Factory\Middleware\Recipe\GetItem::class,
                'Middleware\Recipe\Patch'           => Factory\Middleware\Recipe\Patch::class,
                'Middleware\Recipe\Update'          => Factory\Middleware\Recipe\Update::class,
                Transformer\Recipe::class           => Factory\Transformer\Recipe::class,
            ],
            'input-filters'  => [
                'InputFilter\Recipe\Patch'          => Factory\InputFilter\Recipe\Patch::class,
            ]
        ];
    }
}
