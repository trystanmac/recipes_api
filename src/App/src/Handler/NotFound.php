<?php

declare(strict_types=1);

namespace App\Handler;

use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\EmptyResponse;

/**
 * Class NotFound
 * @package App\Handler
 */
class NotFound implements RequestHandlerInterface
{
    /** @var EmptyResponse $emptyResponse */
    private $emptyResponse;

    /**
     * NotFound constructor.
     * @param EmptyResponse $emptyResponse
     */
    public function __construct(EmptyResponse $emptyResponse)
    {
        $this->emptyResponse = $emptyResponse;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        return $this->emptyResponse
            ->withStatus(StatusCodeInterface::STATUS_NOT_FOUND);
    }
}
