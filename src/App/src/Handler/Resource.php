<?php

declare(strict_types=1);

namespace App\Handler;

use Exception;
use Fig\Http\Message\StatusCodeInterface;
use League\Fractal\Manager as FractalManager;
use League\Fractal\Resource\ResourceInterface as FractalResourceInterface;
use League\Fractal\Scope as FractalScope;
use League\Fractal\TransformerAbstract;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class Resource
 * @package App\Handler\Fractal
 */
class Resource implements RequestHandlerInterface
{
    /** @var FractalManager $fractalManager */
    private $fractalManager;

    /** @var FractalResourceInterface $fractalResourceItem */
    private $fractalResource;

    /** @var JsonResponse $jsonResponse */
    private $jsonResponse;

    /** @var TransformerAbstract $transformer */
    private $transformer;

    /**
     * Item constructor.
     * @param FractalManager $fractalManager
     * @param JsonResponse $jsonResponse
     */
    public function __construct(
        FractalManager $fractalManager,
        JsonResponse $jsonResponse
    ) {
        $this->fractalManager = $fractalManager;
        $this->jsonResponse = $jsonResponse;
    }

    /**
     * @param TransformerAbstract $transformer
     */
    public function setTransformer(TransformerAbstract $transformer): void
    {
        $this->transformer = $transformer;
    }

    public function setFractalResource(FractalResourceInterface $fractalResource): void
    {
        $this->fractalResource = $fractalResource;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws Exception
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        if (null === $this->transformer) {
            throw new Exception('Error, missing transformer');
        }

        /** @var mixed $data */
        $data = $request->getAttribute('Data');

        // If no data has been set as an attribute, we must
        // assume that no data was found
        if (null === $data) {
            return $this->jsonResponse
                ->withStatus(StatusCodeInterface::STATUS_NOT_FOUND);
        }

        $this->fractalResource->setData($data);
        $this->fractalResource->setTransformer($this->transformer);

        /** @var FractalScope $fractalScope */
        $fractalScope = $this->fractalManager->createData($this->fractalResource);

        /** @var array $fractalScopeData */
        $fractalScopeData = $fractalScope->toArray();

        return $this->jsonResponse
            ->withPayload($fractalScopeData)
            ->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
