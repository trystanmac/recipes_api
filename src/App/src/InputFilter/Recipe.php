<?php

declare(strict_types=1);

namespace App\InputFilter;

use Zend\I18n\Validator\Alnum;
use Zend\I18n\Validator\Alpha;
use Zend\InputFilter\InputFilter;
use Zend\Validator\Between;
use Zend\Validator\Regex;

class Recipe extends InputFilter
{
    public function init()
    {
        $this->add([
            'name' => 'cuisine',
            'validators' => [
                [
                    'name' => Alnum::class,
                ]
            ],
            'required' => true,
        ]);

        $this->add([
            'name' => 'slug',
            'validators' => [
                [
                    'name' => Regex::class,
                    'options' => [
                        'pattern' => '/^[\w.-]*$/',
                    ],
                ]
            ],
            'required' => true,
        ]);

        $this->add([
            'name' => 'rating',
            'validators' => [
                [
                    'name' => Between::class,
                    'options' => [
                        'min' => 1,
                        'max' => 5,
                        'inclusive' => true,
                    ],
                ],
            ],
            'required' => false,
        ]);
    }
}
