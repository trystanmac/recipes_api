<?php

declare(strict_types=1);

namespace App\InputFilter\Recipe;

use Zend\I18n\Validator\Alnum;
use Zend\InputFilter\InputFilter;

class Collection extends InputFilter
{
    public function init()
    {
        $this->add([
            'name' => 'cuisine',
            'validators' => [
                [
                    'name' => Alnum::class,
                ]
            ],
            'required' => false,
        ]);
    }
}
