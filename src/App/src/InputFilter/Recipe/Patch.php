<?php

declare(strict_types=1);

namespace App\InputFilter\Recipe;

use Zend\InputFilter\InputFilter;
use Zend\Validator\Between;

class Patch extends InputFilter
{
    public function init()
    {
        $this->add([
            'name' => 'rating',
            'validators' => [
                [
                    'name' => Between::class,
                    'options' => [
                        'min' => 1,
                        'max' => 5,
                        'inclusive' => true,
                    ],
                ],
            ],
            'required' => true,
        ]);
    }
}
