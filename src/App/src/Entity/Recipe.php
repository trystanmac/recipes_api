<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM;

/**
 * Class Recipe
 * @package App\Entity
 *
 * @Entity
 * @Table(name="Recipe",uniqueConstraints={@UniqueConstraint(name="slug_idx", columns={"slug"})})
 */
class Recipe
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="string") **/
    private $cuisine;

    /** @Column(type="string") **/
    private $slug;

    /** @Column(type="integer", nullable=true) **/
    private $rating;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string $cuisine
     * @return Recipe
     */
    public function setCuisine(string $cuisine): self
    {
        $this->cuisine = $cuisine;
        return $this;
    }

    /**
     * @return string
     */
    public function getCuisine(): string
    {
        return $this->cuisine;
    }

    /**
     * @param string $slug
     * @return Recipe
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param int $rating
     * @return Recipe
     */
    public function setRating(?int $rating): self
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRating(): ?int
    {
        return $this->rating;
    }
}
