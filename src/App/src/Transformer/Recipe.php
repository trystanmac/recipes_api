<?php

declare(strict_types=1);

namespace App\Transformer;

use App\Entity\Recipe as RecipeEntity;
use League\Fractal\TransformerAbstract;

/**
 * Class Recipe
 * @package App\Transformer
 */
class Recipe extends TransformerAbstract
{
    /**
     * @param RecipeEntity $recipe
     * @return array
     */
    public function transform(RecipeEntity $recipe): array
    {
        return [
            'id' => $recipe->getId(),
            'cuisine' => $recipe->getCuisine(),
            'slug' => $recipe->getSlug(),
            'rating' => $recipe->getRating(),
        ];
    }
}
