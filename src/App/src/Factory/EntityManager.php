<?php

declare(strict_types=1);

namespace App\Factory;

use Doctrine\ORM\EntityManager as DoctrineEntityManager;
use Doctrine\ORM\ORMException as DoctrineORMException;
use Doctrine\ORM\Tools\Setup as DoctrineToolsSetup;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class EntityManager
 * @package App\Factory
 */
class EntityManager implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DoctrineEntityManager|object
     * @throws DoctrineORMException
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): DoctrineEntityManager
    {
        $config = $container->get('config');

        $entityPaths = $config['doctrine']['entityPaths'];

        $doctrineConfig = DoctrineToolsSetup::createAnnotationMetadataConfiguration($entityPaths);

        $connectionParams = [
            'driver' => $config['doctrine']['driver'],
            'path'   => $config['doctrine']['databasePath'],
        ];

        return DoctrineEntityManager::create($connectionParams, $doctrineConfig);
    }
}
