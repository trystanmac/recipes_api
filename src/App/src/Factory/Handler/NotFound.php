<?php


namespace App\Factory\Handler;

use \App\Handler\NotFound as NotFoundHandler;
use Zend\Diactoros\Response\EmptyResponse;

/**
 * Class NotFound
 * @package App\Factory\Handler
 */
class NotFound
{
    /**
     * @return NotFoundHandler
     */
    public function __invoke(): NotFoundHandler
    {
        return new NotFoundHandler(new EmptyResponse());
    }
}
