<?php

declare(strict_types=1);

namespace App\Factory\Handler;

use App\Handler\Resource as ResourceHandler;
use Interop\Container\ContainerInterface;
use League\Fractal\Resource\Item as FractalResourceItem;
use Zend\ServiceManager\Factory\FactoryInterface;

class ResourceItem implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ResourceHandler
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): ResourceHandler
    {
        /** @var ResourceHandler $resourceHandler */
        $resourceHandler = $container->get(ResourceHandler::class);

        $resourceHandler->setFractalResource(new FractalResourceItem());

        return $resourceHandler;
    }
}
