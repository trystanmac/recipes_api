<?php

declare(strict_types=1);

namespace App\Factory\Handler\Recipe;

use App\Handler\Resource;
use App\Handler\Resource as ResourceHandler;
use App\Transformer\Recipe as RecipeTransformer;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ResourceCollection
 * @package App\Factory\Handler\Recipe
 */
class ResourceCollection implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ResourceHandler
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): ResourceHandler
    {
        /** @var ResourceHandler $resourceHandlerCollection */
        $resourceHandlerCollection = $container->get('Handler\ResourceCollection');

        $recipeTransformer = $container->get(RecipeTransformer::class);

        $resourceHandlerCollection->setTransformer($recipeTransformer);

        return $resourceHandlerCollection;
    }
}
