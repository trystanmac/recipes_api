<?php

declare(strict_types=1);

namespace App\Factory\Handler;

use App\Handler\Resource as ResourceHandler;
use Interop\Container\ContainerInterface;
use League\Fractal\Resource\Collection as FractalResourceCollection;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ResourceCollection
 * @package App\Factory\Handler
 */
class ResourceCollection implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ResourceHandler
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): ResourceHandler
    {
        /** @var ResourceHandler $resourceHandler */
        $resourceHandler = $container->get(ResourceHandler::class);

        $resourceHandler->setFractalResource(new FractalResourceCollection());

        return $resourceHandler;
    }
}
