<?php

declare(strict_types=1);

namespace App\Factory\Handler;

use App\Handler\Resource as ResourceHandler;
use Interop\Container\ContainerInterface;
use League\Fractal\Manager as FractalManager;

use League\Fractal\Serializer\ArraySerializer;
use Zend\Diactoros\Response\JsonResponse;
use Zend\ServiceManager\Factory\FactoryInterface;

class Resource implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ResourceHandler
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): ResourceHandler
    {
        $fractalManager = new FractalManager();
        $fractalManager->setSerializer(new ArraySerializer());

        return new ResourceHandler(
            $fractalManager,
            new JsonResponse([])
        );
    }
}
