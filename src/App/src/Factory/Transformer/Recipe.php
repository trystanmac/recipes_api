<?php

declare(strict_types=1);

namespace App\Factory\Transformer;

use App\Transformer\Recipe as RecipeTransformer;

/**
 * Class Recipe
 * @package App\Factory\Transformer
 */
class Recipe
{
    /**
     * @return RecipeTransformer
     */
    public function __invoke(): RecipeTransformer
    {
        return new RecipeTransformer();
    }
}
