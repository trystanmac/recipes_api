<?php

declare(strict_types=1);

namespace App\Factory\Middleware\Recipe;

use App\Entity\Recipe as RecipeEntity;
use App\Middleware\Recipe\GetItem as RecipeGetItemMiddleware;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class GetItem
 * @package App\Factory\Middleware\Recipe
 */
class GetItem implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RecipeGetItemMiddleware
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): RecipeGetItemMiddleware
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);

        /** @var EntityRepository $entityRepository */
        $entityRepository = $entityManager->getRepository(RecipeEntity::class);

        return new RecipeGetItemMiddleware($entityRepository);
    }
}
