<?php


namespace App\Factory\Middleware\Recipe;

use App\InputFilter\Recipe\Patch as RecipePatchInputFilter;
use App\Middleware\Update as RecipeUpdateMiddleware;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilterPluginManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class Patch
 * @package App\Factory\Middleware\Recipe
 */
class Patch implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RecipeUpdateMiddleware
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): RecipeUpdateMiddleware
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);

        /** @var InputFilterPluginManager $inputFilterPluginManager */
        $inputFilterPluginManager = $container->get(InputFilterPluginManager::class);

        /** @var InputFilterInterface $inputFilter */
        $inputFilter = $inputFilterPluginManager->get(RecipePatchInputFilter::class);

        $inputMethodMapper = [
            'rating' => 'setRating',
        ];

        return new RecipeUpdateMiddleware(
            $inputFilter,
            $entityManager,
            $inputMethodMapper
        );
    }
}
