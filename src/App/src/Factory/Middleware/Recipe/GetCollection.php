<?php

declare(strict_types=1);

namespace App\Factory\Middleware\Recipe;

use App\Entity\Recipe as RecipeEntity;
use App\InputFilter\Recipe\Collection as RecipeCollectionInputFilter;
use App\Middleware\Recipe\GetCollection as RecipesGetCollectionMiddleware;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Interop\Container\ContainerInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilterPluginManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class GetCollection
 * @package App\Factory\Middleware\Recipe
 */
class GetCollection implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RecipesGetCollectionMiddleware
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)// : RecipesGetCollectionMiddleware
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);

        /** @var EntityRepository $entityRepository */
        $entityRepository = $entityManager->getRepository(RecipeEntity::class);

        /** @var InputFilterPluginManager $inputFilterPluginManager */
        $inputFilterPluginManager = $container->get(InputFilterPluginManager::class);

        /** @var InputFilterInterface $inputFilter */
        $inputFilter = $inputFilterPluginManager->get(RecipeCollectionInputFilter::class);

        return new RecipesGetCollectionMiddleware(
            $entityRepository,
            $inputFilter
        );
    }
}
