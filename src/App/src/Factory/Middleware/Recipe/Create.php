<?php

declare(strict_types=1);

namespace App\Factory\Middleware\Recipe;

use App\Entity\Recipe as RecipeEntity;
use App\Middleware\Recipe\Create as CreateRecipeMiddleware;

/**
 * Class Create
 * @package App\Factory\Middleware\Recipe
 */
class Create
{
    /**
     * @return CreateRecipeMiddleware
     */
    public function __invoke(): CreateRecipeMiddleware
    {
        return new CreateRecipeMiddleware(new RecipeEntity());
    }
}
