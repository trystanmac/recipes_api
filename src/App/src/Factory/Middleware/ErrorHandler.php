<?php

declare(strict_types=1);

namespace App\Factory\Middleware;

use App\Middleware\ErrorHandler as ErrorHandlerMiddleware;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class ErrorHandler
 * @package App\Factory\Middleware
 */
class ErrorHandler
{
    /**
     * @return ErrorHandlerMiddleware
     */
    public function __invoke(): ErrorHandlerMiddleware
    {
        return new ErrorHandlerMiddleware(
            new JsonResponse([]),
            new EmptyResponse()
        );
    }
}
