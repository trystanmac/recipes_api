# Zend Expressive

*Begin developing PSR-15 middleware applications in seconds!*

```
Notes:
    The project is in a fully working state although there are a few @todo notes
    in the code.  One thing to note is that as development has progresses, the next 
    step would be to refactor and simplify some of the classes where the tests
    have become more complex. This would be an ongoing practice as part of 
    the lifecycle of the code.

    - The project has been built using Zend Expressive and Docrtine 2 connecting
      to an Sqlite database.
        - The reason for use of Zend Expressive is that I think it provides a nice 
          decoupled way to write modules of code using the MiddlewareInterface
          and RequestHandlerInterface.
        - The reason for the use of Doctrine is that I believe it to be a great abstraction  
          layer from the database allowing the the development of simple php classes that 
          map to the database via annotations. This helps to simplify the code.
        - The reason for using the League\Fractal packages is that it is a very simple way
          to build the data view as a single item and a collection using the same transformer
          to abstract from the underlying data models.
        - Some basic use of Zend Input\Filters have been put in place e.g. to limit the value
          of the rating as per specificayion and validate against non Alphanumric values where
          appropriate. 

    - Only three of the attributes have been used to develop this solution:
        - cuisine
        - slug (unique)
        - rating
      This can be further expanded by adding attributes to the Entity class as well as
      taking care of the transformer for presentation.       
    - Pagination exists, however there is no output currently to show total number
      of records, page number etc...

    @ToDo
        - Pagination from begins from 0. This should start from 1
        - When attempting to create a resource with an existing slug, an execption
          is thrown.  This should be caught and http status 409 (conflict) should be 
          returned.
```

## Getting Started

Navigate to the project route and build the vendor directory and start the docker instance.

```bash
$ composer install
$ docker-compose up -d
```

Once the container has started, log into the container

```bash
$ docker exec -it <containerId> /bin/bash
```

Run the unit tests and setup the database and data

```bash
$ composer setup
```

Update your hosts file

```bash
127.0.0.1       gousto.develop
```

Now you are ready to make some requests to view/create and update the recipes!

Example get collection

```
    http://gousto.develop:8999/recipes
```

Example get collection with pagination

```
    http://gousto.develop:8999/recipes?from=10&num=5
```

Example get collection with filter

```
    http://gousto.develop:8999/recipes?cuisine=asian
```

Example get

```
    http://gousto.develop:8999/recipes/5
```

Example post

```
    http://gousto.develop:8999/recipes
        {
            "cuisine": "asian",
            "slug": "sweet-chilli-and-lime-beef-on-a-crunchy-fresh-noodle-salad",
            "rating": null
        }
```
Example put

```
    http://gousto.develop:8999/recipes/5
        {
            "cuisine": "asian",
            "slug": "sweet-chilli-and-lime-beef-on-a-crunchy-fresh-noodle-salad",
            "rating": null
        }
```
Example patch

```
    http://gousto.develop:8999/recipes/5
        {
            "rating": null
        }
```





