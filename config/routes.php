<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;
use Zend\Expressive\MiddlewareFactory;
use Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware;

/**
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Handler\HomePageHandler::class, 'home');
 * $app->post('/album', App\Handler\AlbumCreateHandler::class, 'album.create');
 * $app->put('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.put');
 * $app->patch('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.patch');
 * $app->delete('/album/:id', App\Handler\AlbumDeleteHandler::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Handler\ContactHandler::class,
 *     Zend\Expressive\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */
return function (Application $app, MiddlewareFactory $factory, ContainerInterface $container) : void
{
    $app->post(
        '/recipes',
        [
            // Returns 201 on success with the full resource body
            BodyParamsMiddleware::class,
            App\Middleware\Recipe\Create::class,
            'Middleware\Recipe\Update',
            'Handler\Recipe\ResourceItem',
        ],
        'recipes.create'
    );

    $app->patch(
        '/recipes/:id',
        [
            // Returns 200 on success with empty body
            BodyParamsMiddleware::class,
            App\Middleware\Recipe\GetItem::class,
            'Middleware\Recipe\Patch',
        ],
        'recipes.patch'
    );

    $app->put(
        '/recipes/:id',
        [
            // Returns 200 on success with empty body
            BodyParamsMiddleware::class,
            App\Middleware\Recipe\GetItem::class,
            'Middleware\Recipe\Update',
        ],
        'recipes.update'
    );

    $app->get(
        '/recipes/:id',
        [
            // Returns 200 on success with the full resource body
            App\Middleware\Recipe\GetItem::class,
            'Handler\Recipe\ResourceItem',
        ],
        'recipes.get.item'
    );

    $app->get(
        '/recipes',
        [
            // Returns 200 on success with a collection of each resource
            App\Middleware\Recipe\GetCollection::class,
            'Handler\Recipe\ResourceCollection',
        ],
        'recipes.get.collection'
    );
};
