!#/bin/bash
cd /var/www/html
vendor/bin/phpunit
touch /tmp/sqlite.test.db
chmod 777 /tmp/sqlite.test.db
vendor/bin/doctrine orm:schema-tool:update --dump-sql
vendor/bin/doctrine orm:schema-tool:update --force
/usr/local/bin/php build/populateDatabase.php