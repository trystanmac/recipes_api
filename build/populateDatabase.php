    <?php

use App\Entity\Recipe as RecipeEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

require_once 'vendor/autoload.php';

$container = require 'config/container.php';

/** @var EntityManager $entityManager */
$entityManager = $container->get(EntityManager::class);

/**
 *
 */
$data = [
    [
        'setCuisine' => 'asian',
        'setSlug' => 'sweet-chilli-and-lime-beef-on-a-crunchy-fresh-noodle-salad',
    ],
    [
        'setCuisine' => 'italian',
        'setSlug' => 'tamil-nadu-prawn-masala',
    ],
    [
        'setCuisine' => 'british',
        'setSlug' => 'umbrian-wild-boar-salami-ragu-with-linguine',
    ],
    [
        'setCuisine' => 'british',
        'setSlug' => 'tenderstem-and-portobello-mushrooms-with-corn-polenta',
    ],
    [
        'setCuisine' => 'british',
        'setSlug' => 'fennel-crusted-pork-with-italian-butter-beans',
    ],
    [
        'setCuisine' => 'asian',
        'setSlug' => 'pork-chilli',
    ],
    [
        'setCuisine' => 'british',
        'setSlug' => 'courgette-pasta-rags',
    ],
    [
        'setCuisine' => 'italian',
        'setSlug' => 'homemade-egg-beansasian',
    ],
    [
        'setCuisine' => 'mediterranean',
        'setSlug' => 'grilled-jerusalem-fish',
    ],
    [
        'setCuisine' => 'mexican',
        'setSlug' => 'pork-katsu-curry',
    ],
];

$entitiesCreated = [];

foreach ($data as $datum) {
    $recipeEntity = new RecipeEntity();

    foreach ($datum as $method => $value) {
//        echo "{$method} => {$value}\n";
        $recipeEntity->$method($value);
    }

    $entityManager->persist($recipeEntity);
    $entitiesCreated[] = $recipeEntity;
}

try {
    $entityManager->flush();
} catch (Exception $e) {
    $msg = $e->getMessage();
    echo "Error, trying to create recipe records:-\n {$msg}\n";
    exit(0);
}

foreach ($entitiesCreated as $entityCreated) {
    echo sprintf(
        'Created %s with id %s and Cuisine %s %s',
        get_class($entityCreated),
        $entityCreated->getId(),
        $entityCreated->getCuisine(),
        "\n"
    );
}