<?php

declare(strict_types=1);

namespace AppTest\Middleware\Recipe;

use App\Entity\Recipe as RecipeEntity;
use App\Middleware\Exception\NotFound;
use App\Middleware\Recipe\GetItem as RecipeGetItemMiddleware;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class RecipeTest
 * @package AppTest\Middleware\Recipe
 */
class GetItemTest extends TestCase
{
    /** @var RecipeGetItemMiddleware $objectUnderTest */
    private $objectUnderTest;

    /** @var MockObject | EntityRepository $entityRepositoryMock */
    private $entityRepositoryMock;

    public function setUp(): void
    {
        $this->entityRepositoryMock = $this->createMock(EntityRepository::class);

        $this->objectUnderTest = new RecipeGetItemMiddleware($this->entityRepositoryMock);
    }

    public function testInstanceOfMiddlewareInterface(): void
    {
        $this->assertInstanceOf(
            MiddlewareInterface::class,
            $this->objectUnderTest
        );
    }

    public function testHandle(): void
    {
        $requestMock = $this->createMock(ServerRequestInterface::class);
        $handlerMock = $this->createMock(RequestHandlerInterface::class);

        $randomId = rand(1000, 2000);
        $requestMock
            ->expects($this->once())
            ->method('getAttribute')
            ->with('id')
            ->willReturn($randomId);

        $recipeEntityMock = $this->createMock(RecipeEntity::class);
        $this->entityRepositoryMock
            ->expects($this->once())
            ->method('find')
            ->with($randomId)
            ->willReturn($recipeEntityMock);

        $requestMockWithAttribute = $this->createMock(ServerRequestInterface::class);
        $requestMock
            ->expects($this->once())
            ->method('withAttribute')
            ->with(
                'Data',
                $this->identicalTo($recipeEntityMock)
            )
            ->willReturn($requestMockWithAttribute);

        $responseFromHandler = $this->createMock(ResponseInterface::class);
        $handlerMock
            ->expects($this->once())
            ->method('handle')
            ->with($this->identicalTo($requestMockWithAttribute))
            ->willReturn($responseFromHandler);

        $this->assertSame(
            $responseFromHandler,
            $this->objectUnderTest->process($requestMock, $handlerMock)
        );
    }

    public function testHandleEntityNotFound(): void
    {
        $requestMock = $this->createMock(ServerRequestInterface::class);
        $handlerMock = $this->createMock(RequestHandlerInterface::class);

        $randomId = rand(1000, 2000);
        $requestMock
            ->expects($this->once())
            ->method('getAttribute')
            ->with('id')
            ->willReturn($randomId);

        $this->entityRepositoryMock
            ->expects($this->once())
            ->method('find')
            ->with($randomId)
            ->willReturn(null);

        $requestMock
            ->expects($this->never())
            ->method('withAttribute');

        $handlerMock
            ->expects($this->never())
            ->method('handle');

        $this->expectException(NotFound::class);

        $this->objectUnderTest->process($requestMock, $handlerMock);
    }
}