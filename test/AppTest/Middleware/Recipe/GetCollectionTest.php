<?php

declare(strict_types=1);

namespace AppTest\Middleware\Recipe;

use App\Entity\Recipe as RecipeEntity;
use App\Middleware\Exception\BadRequest as BadRequestException;
use App\Middleware\Recipe\GetCollection as RecipesGetCollectionMiddleware;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * Class GetCollectionTest
 * @package AppTest\Middleware\Recipe
 */
class GetCollectionTest extends TestCase
{
    /** @var RecipesGetCollectionMiddleware $objectUnderTest */
    private $objectUnderTest;

    /** @var MockObject | EntityRepository $entityRepositoryMock */
    private $entityRepositoryMock;

    /** @var MockObject | InputFilterInterface $inputFilterMock */
    private $inputFilterMock;

    public function setUp(): void
    {
        $this->entityRepositoryMock = $this->createMock(EntityRepository::class);
        $this->inputFilterMock = $this->createMock(InputFilterInterface::class);

        $this->objectUnderTest = new RecipesGetCollectionMiddleware(
            $this->entityRepositoryMock,
            $this->inputFilterMock
        );
    }

    public function testInstanceOfMiddlewareInterface(): void
    {
        $this->assertInstanceOf(
            MiddlewareInterface::class,
            $this->objectUnderTest
        );
    }

    public function testHandle(): void
    {
        $requestMock = $this->createMock(ServerRequestInterface::class);
        $handlerMock = $this->createMock(RequestHandlerInterface::class);

        $queryParams = [
            'num' => rand(100, 199),
            'from' => rand(200, 299),
            'InputFilterValueRandomAttribute' => uniqid(),
        ];
        $requestMock
            ->expects($this->once())
            ->method('getQueryParams')
            ->willReturn($queryParams);

        $this->inputFilterMock
            ->expects($this->once())
            ->method('setData')
            ->with($queryParams);

        $this->inputFilterMock
            ->expects($this->once())
            ->method('isValid')
            ->willReturn(true);

        // Expecting to pass all non null attributes into
        // findBy method
        $filteredData = [
            'attribute1' => uniqid(),
            'attribute2' => uniqid(),
            'attribute3' => null,
        ];
        $this->inputFilterMock
            ->expects($this->once())
            ->method('getValues')
            ->willReturn($filteredData);

        $arrayOfEntitiesReturned = [
            $this->createMock(RecipeEntity::class),
            $this->createMock(RecipeEntity::class),
        ];
        $this->entityRepositoryMock
            ->expects($this->once())
            ->method('findBy')
            ->with(
                [
                    'attribute1' => $filteredData['attribute1'],
                    'attribute2' => $filteredData['attribute2'],
                ],
                ['id' => 'ASC'],
                $queryParams['num'],
                $queryParams['from']
            )
            ->willReturn($arrayOfEntitiesReturned);

        $requestMockWithAttribute = $this->createMock(ServerRequestInterface::class);
        $requestMock
            ->expects($this->once())
            ->method('withAttribute')
            ->with(
                'Data',
                $this->identicalTo($arrayOfEntitiesReturned)
            )
            ->willReturn($requestMockWithAttribute);

        $responseMockFromHandler = $this->createMock(ResponseInterface::class);
        $handlerMock
            ->expects($this->once())
            ->method('handle')
            ->with($this->identicalTo($requestMockWithAttribute))
            ->willReturn($responseMockFromHandler);

        $this->assertSame(
            $responseMockFromHandler,
            $this->objectUnderTest->process($requestMock, $handlerMock)
        );
    }

    public function testHandleInvalidDataFromFilter(): void
    {
        $requestMock = $this->createMock(ServerRequestInterface::class);
        $handlerMock = $this->createMock(RequestHandlerInterface::class);

        $queryParams = [
            'num' => rand(100, 199),
            'from' => rand(200, 299),
            'InputFilterValueRandomAttribute' => uniqid(),
        ];
        $requestMock
            ->expects($this->once())
            ->method('getQueryParams')
            ->willReturn($queryParams);

        $this->inputFilterMock
            ->expects($this->once())
            ->method('setData')
            ->with($queryParams);

        $this->inputFilterMock
            ->expects($this->once())
            ->method('isValid')
            ->willReturn(false);

        $this->inputFilterMock
            ->expects($this->never())
            ->method('getValues');

        $this->entityRepositoryMock
            ->expects($this->never())
            ->method('findBy');

        $requestMock
            ->expects($this->never())
            ->method('withAttribute');

        $handlerMock
            ->expects($this->never())
            ->method('handle');

        $inputFilterMessages = [uniqid()];
        $this->inputFilterMock
            ->expects($this->once())
            ->method('getMessages')
            ->willReturn($inputFilterMessages);

        $this->expectException(BadRequestException::class);
        $this->expectExceptionMessage(json_encode($inputFilterMessages));

        $this->objectUnderTest->process($requestMock, $handlerMock);
    }
}