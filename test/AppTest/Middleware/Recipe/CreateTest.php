<?php


namespace AppTest\Middleware\Recipe;

use App\Entity\Recipe as RecipeEntity;
use App\Middleware\Recipe\Create as CreateRecipeMiddleware;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class CreateTest
 * @package AppTest\Middleware\Recipe
 */
class CreateTest extends TestCase
{
    /** @var CreateRecipeMiddleware $objectUnderTest */
    private $objectUnderTest;

    /** @var RecipeEntity $recipeEntityMock */
    private $recipeEntityMock;

    public function setUp(): void
    {
        $this->recipeEntityMock = $this->createMock(RecipeEntity::class);

        $this->objectUnderTest = new CreateRecipeMiddleware($this->recipeEntityMock);
    }

    public function testInstanceOfMiddlewareInterface(): void
    {
        $this->assertInstanceOf(
            MiddlewareInterface::class,
            $this->objectUnderTest
        );
    }

    public function testProcess()
    {
        $requestMock = $this->createMock(ServerRequestInterface::class);
        $handlerMock = $this->createMock(RequestHandlerInterface::class);

        $requestMockWithAttribute = $this->createMock(ServerRequestInterface::class);
        $requestMock
            ->expects($this->once())
            ->method('withAttribute')
            ->with('Data', $this->identicalTo($this->recipeEntityMock))
            ->willReturn($requestMockWithAttribute);

        $responseMockFromHandler = $this->createMock(ResponseInterface::class);
        $handlerMock
            ->expects($this->once())
            ->method('handle')
            ->with($this->identicalTo($requestMockWithAttribute))
            ->willReturn($responseMockFromHandler);

        $this->assertSame(
            $responseMockFromHandler,
            $this->objectUnderTest->process($requestMock, $handlerMock)
        );
    }
}