<?php


namespace AppTest\Middleware;

use App\Middleware\ErrorHandler as ErrorHandlerMiddleware;
use App\Middleware\Exception\BadRequest as BadRequestException;
use App\Middleware\Exception\NotFound as NotFoundException;
use Exception;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class ErrorHandlerTest
 * @package AppTest\Middleware
 */
class ErrorHandlerTest extends TestCase
{
    /** @var ErrorHandlerMiddleware $objectUnderTest */
    private $objectUnderTest;

    /** @var MockObject | JsonResponse $jsonResponseMock  */
    private $jsonResponseMock;

    /** @var MockObject | EmptyResponse $emptyResponseMock  */
    private $emptyResponseMock;

    public function setUp(): void
    {
        $this->jsonResponseMock = $this->createMock(JsonResponse::class);
        $this->emptyResponseMock = $this->createMock(EmptyResponse::class);

        $this->objectUnderTest = new ErrorHandlerMiddleware(
            $this->jsonResponseMock,
            $this->emptyResponseMock
        );
    }

    public function testInstanceOfMiddlewareInterface(): void
    {
        $this->assertInstanceOf(
            MiddlewareInterface::class,
            $this->objectUnderTest
        );
    }

    public function testProcess(): void
    {
        $requestMock = $this->createMock(ServerRequestInterface::class);
        $handlerMock = $this->createMock(RequestHandlerInterface::class);

        $responseMockFromHandler = $this->createMock(ResponseInterface::class);
        $handlerMock
            ->expects($this->once())
            ->method('handle')
            ->with($this->identicalTo($requestMock))
            ->willReturn($responseMockFromHandler);

        $this->assertSame(
            $responseMockFromHandler,
            $this->objectUnderTest->process($requestMock, $handlerMock)
        );
    }

    /**
     * @param string $errorMsg
     * @param Throwable $throwable
     * @param int $httpStatusCode
     *
     * @dataProvider getTestExceptionData
     */
    public function testProcessThrowsException(
        string $errorMsg,
        Throwable $throwable,
        int $httpStatusCode
    ): void {
        $requestMock = $this->createMock(ServerRequestInterface::class);
        $handlerMock = $this->createMock(RequestHandlerInterface::class);

        $handlerMock
            ->expects($this->once())
            ->method('handle')
            ->with($this->identicalTo($requestMock))
            ->willThrowException($throwable);

        $jsonResponseMockWithPayload = $this->createMock(JsonResponse::class);
        $error = ['error' => $errorMsg];
        $this
            ->jsonResponseMock
            ->expects($this->once())
            ->method('withPayload')
            ->with($error)
            ->willReturn($jsonResponseMockWithPayload);

        $jsonResponseMockWithPayloadAndStatus = $this->createMock(JsonResponse::class);
        $jsonResponseMockWithPayload
            ->expects($this->once())
            ->method('withStatus')
            ->with($httpStatusCode)
            ->willReturn($jsonResponseMockWithPayloadAndStatus);

        $this->assertSame(
            $jsonResponseMockWithPayloadAndStatus,
            $this->objectUnderTest->process($requestMock, $handlerMock)
        );
    }

    public function testProcessThrowsException40(): void {
        $requestMock = $this->createMock(ServerRequestInterface::class);
        $handlerMock = $this->createMock(RequestHandlerInterface::class);

        $throwable = new NotFoundException();
        $httpStatusCode = StatusCodeInterface::STATUS_NOT_FOUND;

        $handlerMock
            ->expects($this->once())
            ->method('handle')
            ->with($this->identicalTo($requestMock))
            ->willThrowException($throwable);

        $this
            ->jsonResponseMock
            ->expects($this->never())
            ->method('withPayload');

        $emptyResponseMockStatus = $this->createMock(JsonResponse::class);
        $this->emptyResponseMock
            ->expects($this->once())
            ->method('withStatus')
            ->with($httpStatusCode)
            ->willReturn($emptyResponseMockStatus);

        $this->assertSame(
            $emptyResponseMockStatus,
            $this->objectUnderTest->process($requestMock, $handlerMock)
        );
    }

    /**
     * @return array
     */
    public function getTestExceptionData(): array
    {
        $errorMsg = uniqid();

        return [
            [$errorMsg, new BadRequestException($errorMsg), StatusCodeInterface::STATUS_BAD_REQUEST],
            [$errorMsg, new Exception($errorMsg), StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR],
        ];
    }
}