<?php

declare(strict_types=1);

namespace AppTest\Middleware;

use App\Entity\Recipe as RecipeEntity;
use App\Middleware\Exception\BadRequest as BadRequestException;
use App\Middleware\Update as UpdateMiddleware;
use Doctrine\ORM\EntityManager;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\InputFilter\InputFilterInterface;

class UpdateTest extends TestCase
{
    /** @var UpdateMiddleware $objectUnderTest */
    private $objectUnderTest;

    /** @var MockObject | RecipeEntity $recipeEntityMock */
    private $recipeEntityMock;

    /** @var MockObject | InputFilterInterface $inputFilterMock  */
    private $inputFilterMock;

    /** @var MockObject | EntityManager $entityManagerMock  */
    private $entityManagerMock;

    /** @var MockObject | EntityManager $entityManagerMock  */
    private $inputMethodMapper;

    public function setUp(): void
    {
        $this->recipeEntityMock = $this->createMock(RecipeEntity::class);
        $this->inputFilterMock = $this->createMock(InputFilterInterface::class);
        $this->entityManagerMock = $this->createMock(EntityManager::class);
        $this->inputMethodMapper = [
            'cuisine' => 'setCuisine',
            'slug' => 'setSlug',
            'rating' => 'setRating',
        ];

        $this->objectUnderTest = new UpdateMiddleware(
            $this->inputFilterMock,
            $this->entityManagerMock,
            $this->inputMethodMapper
        );
    }

    public function testImplementsMiddlewareInterface(): void
    {
        $this->assertInstanceOf(
            MiddlewareInterface::class,
            $this->objectUnderTest
        );
    }

    public function testProcessWithCreateStatus()
    {
        $requestMock = $this->createMock(ServerRequestInterface::class);
        $handlerMock = $this->createMock(RequestHandlerInterface::class);

        $requestMock
            ->expects($this->once())
            ->method('getAttribute')
            ->with('Data')
            ->willReturn($this->recipeEntityMock);

        $parsedBody = [uniqid()];
        $requestMock
            ->expects($this->once())
            ->method('getParsedBody')
            ->willReturn($parsedBody);

        $this->inputFilterMock
            ->expects($this->once())
            ->method('setData')
            ->with($parsedBody);

        $this->inputFilterMock
            ->expects($this->once())
            ->method('isValid')
            ->willReturn(true);

        $valuesFromInputFilter = [
            'cuisine' => uniqid(),
            'slug' => uniqid(),
            'rating' => rand(1000, 2000),
        ];
        $this->inputFilterMock
            ->expects($this->once())
            ->method('getValues')
            ->willReturn($valuesFromInputFilter);

        foreach ($this->inputMethodMapper as $attribute => $method) {
            $this->recipeEntityMock
                ->expects($this->once())
                ->method($method)
                ->with($valuesFromInputFilter[$attribute])
                ->willReturnSelf();
        }

        $this->entityManagerMock
            ->expects($this->at(0))
            ->method('persist')
            ->with($this->identicalTo($this->recipeEntityMock));

        $this->entityManagerMock
            ->expects($this->at(1))
            ->method('flush');

        $requestMockWithAttribute = $this->createMock(ServerRequestInterface::class);
        $requestMock
            ->expects($this->once())
            ->method('withAttribute')
            ->with('Data', $this->identicalTo($this->recipeEntityMock))
            ->willReturn($requestMockWithAttribute);

        $responseFromHandler = $this->createMock(ResponseInterface::class);
        $handlerMock
            ->expects($this->once())
            ->method('handle')
            ->with($this->identicalTo($requestMockWithAttribute))
            ->willReturn($responseFromHandler);

        $responseFromHandlerWithStatus = $this->createMock(ResponseInterface::class);
        $responseFromHandler
            ->expects($this->once())
            ->method('withStatus')
            ->with(StatusCodeInterface::STATUS_CREATED)
            ->willReturn($responseFromHandlerWithStatus);

        $this->assertSame(
            $responseFromHandlerWithStatus,
            $this->objectUnderTest->process($requestMock, $handlerMock)
        );
    }

    public function testProcessWithUpdateStatus()
    {
        $requestMock = $this->createMock(ServerRequestInterface::class);
        $handlerMock = $this->createMock(RequestHandlerInterface::class);

        $this->recipeEntityMock
            ->expects($this->once())
            ->method('getId')
            ->willReturn(rand(1000, 2000));

        $requestMock
            ->expects($this->once())
            ->method('getAttribute')
            ->with('Data')
            ->willReturn($this->recipeEntityMock);

        $parsedBody = [uniqid()];
        $requestMock
            ->expects($this->once())
            ->method('getParsedBody')
            ->willReturn($parsedBody);

        $this->inputFilterMock
            ->expects($this->once())
            ->method('setData')
            ->with($parsedBody);

        $this->inputFilterMock
            ->expects($this->once())
            ->method('isValid')
            ->willReturn(true);

        $valuesFromInputFilter = [
            'cuisine' => uniqid(),
            'slug' => uniqid(),
            'rating' => rand(1000, 2000),
        ];
        $this->inputFilterMock
            ->expects($this->once())
            ->method('getValues')
            ->willReturn($valuesFromInputFilter);

        $attributeMethodMapper = [
            'cuisine' => 'setCuisine',
            'slug' => 'setSlug',
            'rating' => 'setRating',
        ];

        foreach ($attributeMethodMapper as $attribute => $method) {
            $this->recipeEntityMock
                ->expects($this->once())
                ->method($method)
                ->with(
                    $valuesFromInputFilter[$attribute]
                )
                    ->willReturnSelf();
        }

        $this->entityManagerMock
            ->expects($this->at(0))
            ->method('persist')
            ->with($this->identicalTo($this->recipeEntityMock));

        $this->entityManagerMock
            ->expects($this->at(1))
            ->method('flush');

        $requestMockWithAttribute = $this->createMock(ServerRequestInterface::class);
        $requestMock
            ->expects($this->once())
            ->method('withAttribute')
            ->with('Data', $this->identicalTo($this->recipeEntityMock))
            ->willReturn($requestMockWithAttribute);

        $responseFromHandler = $this->createMock(ResponseInterface::class);
        $handlerMock
            ->expects($this->once())
            ->method('handle')
            ->with($this->identicalTo($requestMockWithAttribute))
            ->willReturn($responseFromHandler);

        $responseFromHandlerWithStatus = $this->createMock(ResponseInterface::class);
        $responseFromHandler
            ->expects($this->once())
            ->method('withStatus')
            ->with(StatusCodeInterface::STATUS_OK)
            ->willReturn($responseFromHandlerWithStatus);

        $this->assertSame(
            $responseFromHandlerWithStatus,
            $this->objectUnderTest->process($requestMock, $handlerMock)
        );
    }

    public function testProcessInvalidData(): void
    {
        $requestMock = $this->createMock(ServerRequestInterface::class);
        $handlerMock = $this->createMock(RequestHandlerInterface::class);

        $requestMock
            ->expects($this->once())
            ->method('getAttribute')
            ->with('Data')
            ->willReturn($this->recipeEntityMock);

        $parsedBody = [uniqid()];
        $requestMock
            ->expects($this->once())
            ->method('getParsedBody')
            ->willReturn($parsedBody);

        $this->inputFilterMock
            ->expects($this->once())
            ->method('setData')
            ->with($parsedBody);

        $this->inputFilterMock
            ->expects($this->once())
            ->method('isValid')
            ->willReturn(false);

        $this->inputFilterMock
            ->expects($this->never())
            ->method('getValues');

        $this->recipeEntityMock
            ->expects($this->never())
            ->method('setCuisine');

        $this->recipeEntityMock
            ->expects($this->never())
            ->method('setSlug');

        $this->recipeEntityMock
            ->expects($this->never())
            ->method('setRating');

        $this->entityManagerMock
            ->expects($this->never())
            ->method('persist');

        $this->entityManagerMock
            ->expects($this->never())
            ->method('flush');

        $inputFilterMessages = [uniqid()];
        $this->inputFilterMock
            ->expects($this->once())
            ->method('getMessages')
            ->willReturn($inputFilterMessages);

        $this->expectException(BadRequestException::class);
        $this->expectExceptionMessage(json_encode($inputFilterMessages));

        $this->objectUnderTest->process($requestMock, $handlerMock);
    }
}