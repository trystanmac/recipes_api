<?php

declare(strict_types=1);

namespace AppTest\Handler;

use App\Handler\Resource as ResourceHandler;
use Exception;
use Fig\Http\Message\StatusCodeInterface;
use League\Fractal\Manager as FractalManager;
use League\Fractal\Resource\ResourceInterface as FractalResourceInterface;
use League\Fractal\Scope as FractalScope;
use League\Fractal\TransformerAbstract;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class ResourceTest
 * @package AppTest\Handler\Fractal
 */
class ResourceTest extends TestCase
{
    /** @var ResourceHandler $objectUnderTest */
    private $objectUnderTest;

    /** @var MockObject | JsonResponse $jsonResponseMock */
    private $jsonResponseMock;

    /** @var MockObject | FractalManager $fractalManagerMock */
    private $fractalManagerMock;

    /** @var MockObject | TransformerAbstract $transformerMock */
    private $transformerMock;

    public function setUp(): void
    {
        $this->fractalManagerMock = $this->createMock(FractalManager::class);
        $this->transformerMock = $this->createMock(TransformerAbstract::class);
        $this->jsonResponseMock = $this->createMock(JsonResponse::class);

        $this->objectUnderTest = new ResourceHandler(
            $this->fractalManagerMock,
            $this->jsonResponseMock
        );
    }

    public function testInstanceOfRequestHandlerInterface(): void
    {
        $this->assertInstanceOf(
            RequestHandlerInterface::class,
            $this->objectUnderTest
        );
    }

    public function testHandleWithoutTransformer(): void
    {
        $requestMock = $this->createMock(ServerRequestInterface::class);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Error, missing transformer');

        $this->objectUnderTest->handle($requestMock);
    }

    public function testHandleWithoutWithoutDataSet(): void
    {
        $requestMock = $this->createMock(ServerRequestInterface::class);

        $transformerMock = $this->createMock(TransformerAbstract::class);
        $this->objectUnderTest->setTransformer($transformerMock);

        $fractalResourceMock = $this->createMock(FractalResourceInterface::class);
        $this->objectUnderTest->setFractalResource($fractalResourceMock);

        $requestMock
            ->expects($this->once())
            ->method('getAttribute')
            ->with('Data')
            ->willReturn(null);

        $fractalResourceMock
            ->expects($this->never())
            ->method('setData');

        $fractalResourceMock
            ->expects($this->never())
            ->method('setTransformer');

        $this->fractalManagerMock
            ->expects($this->never())
            ->method('createData');

        $jsonResponseMockWithStatus = $this->createMock(JsonResponse::class);
        $this->jsonResponseMock
            ->expects($this->once())
            ->method('withStatus')
            ->with(StatusCodeInterface::STATUS_NOT_FOUND)
            ->willReturn($jsonResponseMockWithStatus);

        $this->assertSame(
            $jsonResponseMockWithStatus,
            $this->objectUnderTest->handle($requestMock)#
        );
    }

    public function testHandle(): void
    {
        $requestMock = $this->createMock(ServerRequestInterface::class);

        $transformerMock = $this->createMock(TransformerAbstract::class);
        $this->objectUnderTest->setTransformer($transformerMock);

        $fractalResourceMock = $this->createMock(FractalResourceInterface::class);
        $this->objectUnderTest->setFractalResource($fractalResourceMock);

        $dataToTransform = [uniqid()];
        $requestMock
            ->expects($this->once())
            ->method('getAttribute')
            ->with('Data')
            ->willReturn($dataToTransform);

        $fractalResourceMock
            ->expects($this->once())
            ->method('setData')
            ->with($this->identicalTo($dataToTransform));

        $fractalResourceMock
            ->expects($this->once())
            ->method('setTransformer')
            ->with($this->identicalTo($transformerMock));

        $fractalScopeMock = $this->createMock(FractalScope::class);
        $this->fractalManagerMock
            ->expects($this->once())
            ->method('createData')
            ->with($this->identicalTo($fractalResourceMock))
            ->willReturn($fractalScopeMock);

        $fractalScopeData = [uniqid()];
        $fractalScopeMock
            ->expects($this->once())
            ->method('toArray')
            ->willReturn($fractalScopeData);

        $jsonResponseMockWithPayload = $this->createMock(JsonResponse::class);
        $this->jsonResponseMock
            ->expects($this->once())
            ->method('withPayload')
            ->with($fractalScopeData)
            ->willReturn($jsonResponseMockWithPayload);

        $jsonResponseMockWithStatus = $this->createMock(JsonResponse::class);
        $jsonResponseMockWithPayload
            ->expects($this->once())
            ->method('withStatus')
            ->with(StatusCodeInterface::STATUS_OK)
            ->willReturn($jsonResponseMockWithStatus);

        $this->assertSame(
            $jsonResponseMockWithStatus,
            $this->objectUnderTest->handle($requestMock)
        );
    }
}