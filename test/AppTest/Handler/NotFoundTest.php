<?php

declare(strict_types=1);

namespace AppTest\Handler;

use App\Handler\NotFound;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\EmptyResponse;

class NotFoundTest extends TestCase
{
    private $objectUnderTest;

    /** @var MockObject | EmptyResponse $emptyResponseMock */
    private $emptyResponseMock;

    public function setUp(): void
    {
        $this->emptyResponseMock = $this->createMock(EmptyResponse::class);
        $this->objectUnderTest = new NotFound($this->emptyResponseMock);
    }

    public function testInstanceOfRequestHandlerInterface(): void
    {
        $this->assertInstanceOf(
            RequestHandlerInterface::class,
            $this->objectUnderTest
        );
    }

    public function testHandle(): void
    {
        $requestMock = $this->createMock(ServerRequestInterface::class);

        $emptyResponseMockWithStatus404 = $this->createMock(EmptyResponse::class);
        $this->emptyResponseMock
            ->expects($this->once())
            ->method('withStatus')
            ->with(StatusCodeInterface::STATUS_NOT_FOUND)
            ->willReturn($emptyResponseMockWithStatus404);

        $this->assertSame(
            $emptyResponseMockWithStatus404,
            $this->objectUnderTest->handle($requestMock)
        );
    }
}