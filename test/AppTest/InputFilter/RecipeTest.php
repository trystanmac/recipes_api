<?php

declare(strict_types=1);

namespace AppTest\InputFilter;

use App\InputFilter\Recipe as RecipeInputFilter;
use PHPUnit\Framework\TestCase;
use Zend\I18n\Validator\Alnum as AlnumValidator;
use Zend\InputFilter\InputFilter;
use Zend\Validator\Between;
use Zend\Validator\Regex;

class RecipeTest extends TestCase
{
    private $objectUnderTest;

    public function setUp()
    {
        $this->objectUnderTest = new RecipeInputFilter();
        $this->objectUnderTest->init();
    }

    public function testIsInstanceOfInputFiler()
    {
        $this->assertInstanceOf(
            InputFilter::class,
            $this->objectUnderTest
        );
    }

    /**
     * @param $string
     * @param $fieldToCheck
     * @param $errorKey
     * @param $errorMsg
     *
     * @dataProvider getTestData
     */
    public function testNonAlphanumericValues(
        $string,
        $fieldToCheck,
        $errorKey,
        $errorMsg
    ): void {

        $dataToSet = [
            'cuisine' => uniqid(),
            'slug' => uniqid(),
        ];

        $dataToSet[$fieldToCheck] = $string;

        $this->objectUnderTest->setData($dataToSet);

        $this->assertFalse($this->objectUnderTest->isValid());

        $errorMessages = $this->objectUnderTest->getMessages();

        $this->assertContains(
            $errorMsg,
            $errorMessages[$fieldToCheck][$errorKey],
            json_encode($errorMessages)
        );
    }

    public function getTestData(): array
    {
        $string = "!'£$%`^&&";

        return [
            [$string, 'cuisine', AlnumValidator::NOT_ALNUM, 'The input contains characters which are non alphabetic and no digits'],
            [$string, 'slug', Regex::NOT_MATCH, "The input does not match against pattern"],
            [0, 'rating', Between::NOT_BETWEEN, "The input is not between '1' and '5', inclusively"],
            [6, 'rating', Between::NOT_BETWEEN, "The input is not between '1' and '5', inclusively"],
        ];
    }

    public function testSlug(): void
    {
        $dataToSet = [
            'cuisine' => uniqid(),
            'slug' => sprintf('%s-%s', uniqid(), uniqid()),
        ];

        $this->objectUnderTest->setData($dataToSet);
        $this->objectUnderTest->isValid();
        $this->assertTrue($this->objectUnderTest->isValid());
        $messages = $this->objectUnderTest->getMessages();
        $this->assertEmpty($messages, json_encode($dataToSet));
    }
}