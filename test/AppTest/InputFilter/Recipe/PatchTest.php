<?php

declare(strict_types=1);

namespace AppTest\InputFilter;

use App\InputFilter\Recipe\Patch as RecipePatchInputFilter;
use PHPUnit\Framework\TestCase;
use Zend\InputFilter\InputFilter;
use Zend\Validator\Between;

class PatchTest extends TestCase
{
    private $objectUnderTest;

    public function setUp()
    {
        $this->objectUnderTest = new RecipePatchInputFilter();
        $this->objectUnderTest->init();
    }

    public function testIsInstanceOfInputFiler()
    {
        $this->assertInstanceOf(
            InputFilter::class,
            $this->objectUnderTest
        );
    }

    /**
     * @param $string
     * @param $fieldToCheck
     * @param $errorKey
     * @param $errorMsg
     *
     * @dataProvider getTestData
     */
    public function testNonAlphanumericValues(
        $string,
        $fieldToCheck,
        $errorKey,
        $errorMsg
    ): void {

        $dataToSet = [
            'cuisine' => uniqid(),
            'slug' => uniqid(),
        ];

        $dataToSet[$fieldToCheck] = $string;

        $this->objectUnderTest->setData($dataToSet);

        $this->assertFalse($this->objectUnderTest->isValid());

        $errorMessages = $this->objectUnderTest->getMessages();

        $this->assertContains(
            $errorMsg,
            $errorMessages[$fieldToCheck][$errorKey],
            json_encode($errorMessages)
        );
    }

    public function getTestData(): array
    {
        return [
            [0, 'rating', Between::NOT_BETWEEN, "The input is not between '1' and '5', inclusively"],
            [6, 'rating', Between::NOT_BETWEEN, "The input is not between '1' and '5', inclusively"],
        ];
    }
}