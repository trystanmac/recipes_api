<?php

declare(strict_types=1);

namespace AppTest\InputFilter;

use App\InputFilter\Recipe\Collection as CollectionRecipeInputFilter;
use PHPUnit\Framework\TestCase;
use Zend\I18n\Validator\Alnum as AlnumValidator;
use Zend\InputFilter\InputFilter;


class CollectionTest extends TestCase
{
    private $objectUnderTest;

    public function setUp()
    {
        $this->objectUnderTest = new CollectionRecipeInputFilter();
        $this->objectUnderTest->init();
    }

    public function testIsInstanceOfInputFiler()
    {
        $this->assertInstanceOf(
            InputFilter::class,
            $this->objectUnderTest
        );
    }
    /**
     * @param $string
     * @param $fieldToCheck
     * @param $errorKey
     * @param $errorMsg
     *
     * @dataProvider getTestData
     */
    public function testNonAlphanumericValues(
        $string,
        $fieldToCheck,
        $errorKey,
        $errorMsg
    ): void {

        $dataToSet = [
            'cuisine' => uniqid(),
            'slug' => uniqid(),
        ];

        $dataToSet[$fieldToCheck] = $string;

        $this->objectUnderTest->setData($dataToSet);

        $this->assertFalse($this->objectUnderTest->isValid());

        $errorMessages = $this->objectUnderTest->getMessages();

        $this->assertContains(
            $errorMsg,
            $errorMessages[$fieldToCheck][$errorKey],
            json_encode($errorMessages)
        );
    }

    public function getTestData(): array
    {
        $string = "!'£$%`^&&";

        return [
            [$string, 'cuisine', AlnumValidator::NOT_ALNUM, 'The input contains characters which are non alphabetic and no digits'],
        ];
    }
}