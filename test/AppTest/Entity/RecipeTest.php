<?php
declare(strict_types=1);

namespace AppTest\Entity;

use App\Entity\Recipe;
use PHPUnit\Framework\TestCase;

/**
 * Class RecipeTest
 * @package AppTest\Entity
 */
class RecipeTest extends TestCase
{
    private $objectUnderTest;

    public function setUp()
    {
        $this->objectUnderTest = new Recipe();
    }

    public function testSetCuisineFluidInterface(): void
    {
        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->setCuisine(uniqid())
        );
    }

    public function testSetAndGetCuisine(): void
    {
        $cuisine = uniqid();
        $this->objectUnderTest->setCuisine($cuisine);

        $this->assertEquals(
            $cuisine,
            $this->objectUnderTest->getCuisine()
        );
    }

    public function testSetSlugFluidInterface(): void
    {
        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->setSlug(uniqid())
        );
    }

    public function testSetAndGetSlug(): void
    {
        $slug = uniqid();
        $this->objectUnderTest->setSlug($slug);

        $this->assertEquals(
            $slug,
            $this->objectUnderTest->getSlug()
        );
    }

    public function testSetRatingFluidInterface(): void
    {
        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->setRating(rand(1000, 2000))
        );
    }

    public function testSetAndGetRating(): void
    {
        $rating = rand(1000, 2000);
        $this->objectUnderTest->setRating($rating);

        $this->assertEquals(
            $rating,
            $this->objectUnderTest->getRating()
        );
    }

    public function testNullRating(): void
    {
        $this->assertNull($this->objectUnderTest->getRating());
    }

    public function testSetNullRating(): void
    {
        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->setRating(null)
        );
    }

    public function testGetId(): void
    {
        // Note, this id should never be allowed to be
        // set on the entity. When the entity is persited
        // to the database, an id should be auto generated
        $this->assertNull($this->objectUnderTest->getId());
    }
}