<?php


namespace AppTest\Factory\Middleware\Recipe;

use App\Factory\Middleware\Recipe\Patch as RecipePatchFactory;
use App\InputFilter\Recipe\Patch as RecipePatchInputFilter;
use App\Middleware\Update as UpdateMiddleware;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilterPluginManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class PatchTest
 * @package AppTest\Factory\Middleware\Recipe
 */
class PatchTest extends TestCase
{
    /** @var RecipePatchFactory $objectUnderTest */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new RecipePatchFactory();
    }

    public function testImplementsFactoryInterface(): void
    {
        $this->assertInstanceOf(
            FactoryInterface::class,
            $this->objectUnderTest
        );
    }

    public function testInvoke(): void
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $entityManagerMock = $this->createMock(EntityManager::class);
        $inputFilterPluginManagerMock = $this->createMock(InputFilterPluginManager::class);

        $containerMock
            ->expects($this->any())
            ->method('get')
            ->willReturnMap([
                [EntityManager::class, $entityManagerMock],
                [InputFilterPluginManager::class, $inputFilterPluginManagerMock],
            ]);

        $inputFilterMock = $this->createMock(InputFilterInterface::class);
        $inputFilterPluginManagerMock
            ->expects($this->once())
            ->method('get')
            ->with(RecipePatchInputFilter::class)
            ->willReturn($inputFilterMock);

        $this->assertInstanceOf(
            UpdateMiddleware::class,
            $this->objectUnderTest->__invoke($containerMock, 'some name', [])
        );
    }
}