<?php


namespace AppTest\Factory\Middleware\Recipe;

use App\Factory\Middleware\Recipe\Create as CreateRecipeMiddlewareFactory;
use App\Middleware\Recipe\Create as CreateRecipeMiddleware;
use PHPUnit\Framework\TestCase;

/**
 * Class CreateTest
 * @package AppTest\Factory\Middleware\Recipe
 */
class CreateTest extends TestCase
{
    /** @var CreateRecipeMiddlewareFactory $objectUnderTest */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new CreateRecipeMiddlewareFactory();
    }

    public function testInvoke(): void
    {
        $this->assertInstanceOf(
            CreateRecipeMiddleware::class,
            $this->objectUnderTest->__invoke()
        );
    }
}