<?php


namespace AppTest\Factory\Middleware\Recipe;

use App\Entity\Recipe;
use App\Factory\Middleware\Recipe\GetItem as RecipeGetItemFactory;
use App\Middleware\Recipe\GetItem as RecipeGetItemMiddleware;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class GetItemTest
 * @package AppTest\Factory\Middleware\Recipe
 */
class GetItemTest extends TestCase
{
    /** @var RecipeGetItemFactory $objectUnderTest */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new RecipeGetItemFactory();
    }

    public function testImplementsFactoryInterface(): void
    {
        $this->assertInstanceOf(
            FactoryInterface::class,
            $this->objectUnderTest
        );
    }

    public function testInvoke(): void
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $entityManagerMock = $this->createMock(EntityManager::class);

        $containerMock
            ->expects($this->any())
            ->method('get')
            ->willReturnMap([
                [EntityManager::class, $entityManagerMock],
            ]);

        $entityRepositoryMock = $this->createMock(EntityRepository::class);
        $entityManagerMock
            ->expects($this->once())
            ->method('getRepository')
            ->with(Recipe::class)
            ->willReturn($entityRepositoryMock);

        $this->assertInstanceOf(
            RecipeGetItemMiddleware::class,
            $this->objectUnderTest->__invoke($containerMock, 'some name', [])
        );
    }
}