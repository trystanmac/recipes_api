<?php


namespace AppTest\Factory\Middleware\Recipe;

use App\Factory\Middleware\Recipe\Update as RecipeUpdateFactory;
use App\InputFilter\Recipe as RecipeInputFilter;
use App\Middleware\Update as UpdateMiddleware;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilterPluginManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class UpdateTest
 * @package AppTest\Factory\Middleware\Recipe
 */
class UpdateTest extends TestCase
{
    /** @var RecipeUpdateFactory $objectUnderTest */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new RecipeUpdateFactory();
    }

    public function testImplementsFactoryInterface(): void
    {
        $this->assertInstanceOf(
            FactoryInterface::class,
            $this->objectUnderTest
        );
    }

    public function testInvoke(): void
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $entityManagerMock = $this->createMock(EntityManager::class);
        $inputFilterPluginManagerMock = $this->createMock(InputFilterPluginManager::class);

        $containerMock
            ->expects($this->any())
            ->method('get')
            ->willReturnMap([
                [EntityManager::class, $entityManagerMock],
                [InputFilterPluginManager::class, $inputFilterPluginManagerMock],
            ]);

        $inputFilterMock = $this->createMock(InputFilterInterface::class);
        $inputFilterPluginManagerMock
            ->expects($this->once())
            ->method('get')
            ->with(RecipeInputFilter::class)
            ->willReturn($inputFilterMock);

        $this->assertInstanceOf(
            UpdateMiddleware::class,
            $this->objectUnderTest->__invoke($containerMock, 'some name', [])
        );
    }
}