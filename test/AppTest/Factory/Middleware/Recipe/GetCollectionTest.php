<?php


namespace AppTest\Factory\Middleware\Recipe;

use App\Entity\Recipe;
use App\Factory\Middleware\Recipe\GetCollection as RecipeGetCollectionFactory;
use App\InputFilter\Recipe\Collection as RecipeCollectionInputFilter;
use App\Middleware\Recipe\GetCollection as RecipesGetCollectionMiddleware;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilterPluginManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class GetCollectionTest
 * @package AppTest\Factory\Middleware\Recipe
 */
class GetCollectionTest extends TestCase
{
    /** @var RecipeGetCollectionFactory $objectUnderTest */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new RecipeGetCollectionFactory();
    }

    public function testImplementsFactoryInterface(): void
    {
        $this->assertInstanceOf(
            FactoryInterface::class,
            $this->objectUnderTest
        );
    }

    public function testInvoke(): void
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $entityManagerMock = $this->createMock(EntityManager::class);
        $inputFilterPluginManagerMock = $this->createMock(InputFilterPluginManager::class);

        $containerMock
            ->expects($this->any())
            ->method('get')
            ->willReturnMap([
                [EntityManager::class, $entityManagerMock],
                [InputFilterPluginManager::class, $inputFilterPluginManagerMock],
            ]);

        $entityRepositoryMock = $this->createMock(EntityRepository::class);
        $entityManagerMock
            ->expects($this->once())
            ->method('getRepository')
            ->with(Recipe::class)
            ->willReturn($entityRepositoryMock);

        $inputFilterMock = $this->createMock(InputFilterInterface::class);
        $inputFilterPluginManagerMock
            ->expects($this->once())
            ->method('get')
            ->with(RecipeCollectionInputFilter::class)
            ->willReturn($inputFilterMock);

        $this->assertInstanceOf(
            RecipesGetCollectionMiddleware::class,
            $this->objectUnderTest->__invoke($containerMock, 'some name', [])
        );
    }
}