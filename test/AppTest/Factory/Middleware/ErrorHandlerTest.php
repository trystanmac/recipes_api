<?php


namespace AppTest\Factory\Middleware;

use App\Factory\Middleware\ErrorHandler as ErrorHandlerMiddlewareFactory;
use App\Middleware\ErrorHandler as ErrorHandlerMiddleware;
use PHPUnit\Framework\TestCase;

/**
 * Class ErrorHandlerTest
 * @package AppTest\Factory\Middleware\Recipe
 */
class ErrorHandlerTest extends TestCase
{
    /** @var ErrorHandlerMiddlewareFactory $objectUnderTest */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new ErrorHandlerMiddlewareFactory();
    }

    public function testInvoke(): void
    {
        $this->assertInstanceOf(
            ErrorHandlerMiddleware::class,
            $this->objectUnderTest->__invoke()
        );
    }
}