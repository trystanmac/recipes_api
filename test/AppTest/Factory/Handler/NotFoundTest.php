<?php


namespace AppTest\Factory\Handler;

use App\Handler\NotFound as NotFoundHandler;
use App\Factory\Handler\NotFound as NotFoundFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class NotFoundTest
 * @package AppTest\Factory\Handler
 */
class NotFoundTest extends TestCase
{
    /** @var NotFoundFactory $objectUnderTest */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new NotFoundFactory();
    }

    public function testInvoke(): void
    {
        $this->assertInstanceOf(
            NotFoundHandler::class,
            $this->objectUnderTest->__invoke()
        );
    }
}