<?php


namespace AppTest\Factory\Handler;

use App\Handler\Resource as ResourceHandler;
use App\Factory\Handler\Resource as ResourceFactory;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class NotFoundTest
 * @package AppTest\Factory\Handler
 */
class ResourceTest extends TestCase
{
    /** @var ResourceFactory $objectUnderTest */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new ResourceFactory();
    }

    public function testImplementsFactoryInterface(): void
    {
        $this->assertInstanceOf(
            FactoryInterface::class,
            $this->objectUnderTest
        );
    }

    public function testInvoke(): void
    {
        $containerMock = $this->createMock(ContainerInterface::class);

        $this->assertInstanceOf(
            ResourceHandler::class,
            $this->objectUnderTest->__invoke($containerMock, 'some name', [])
        );
    }
}