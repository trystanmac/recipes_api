<?php


namespace AppTest\Factory\Handler\Recipe;

use App\Handler\Resource as ResourceHandler;
use App\Factory\Handler\Recipe\ResourceCollection as RecipeResourceCollectionFactory;
use App\Transformer\Recipe as RecipeTransformer;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ResourceCollectionTest
 * @package AppTest\Factory\Handler\Recipe
 */
class ResourceCollectionTest extends TestCase
{
    /** @var RecipeResourceCollectionFactory $objectUnderTest */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new RecipeResourceCollectionFactory();
    }

    public function testImplementsFactoryInterface(): void
    {
        $this->assertInstanceOf(
            FactoryInterface::class,
            $this->objectUnderTest
        );
    }

    public function testInvoke(): void
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $resourceHandlerMock = $this->createMock(ResourceHandler::class);
        $recipeTransformerMock = $this->createMock(RecipeTransformer::class);

        $containerMock
            ->expects($this->any())
            ->method('get')
            ->willReturnMap([
                ['Handler\ResourceCollection', $resourceHandlerMock],
                [RecipeTransformer::class, $recipeTransformerMock],
            ]);

        $resourceHandlerMock
            ->expects($this->once())
            ->method('setTransformer')
            ->with($this->identicalTo($recipeTransformerMock));

        $this->assertSame(
            $resourceHandlerMock,
            $this->objectUnderTest->__invoke($containerMock, 'some name', [])
        );
    }
}