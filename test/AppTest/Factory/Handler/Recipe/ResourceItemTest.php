<?php


namespace AppTest\Factory\Handler\Recipe;

use App\Handler\Resource as ResourceHandler;
use App\Factory\Handler\Recipe\ResourceItem as RecipeResourceItemFactory;
use App\Transformer\Recipe as RecipeTransformer;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class NotFoundTest
 * @package AppTest\Factory\Handler
 */
class ResourceItemTest extends TestCase
{
    /** @var RecipeResourceItemFactory $objectUnderTest */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new RecipeResourceItemFactory();
    }

    public function testImplementsFactoryInterface(): void
    {
        $this->assertInstanceOf(
            FactoryInterface::class,
            $this->objectUnderTest
        );
    }

    public function testInvoke(): void
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $resourceHandlerMock = $this->createMock(ResourceHandler::class);
        $recipeTransformerMock = $this->createMock(RecipeTransformer::class);

        $containerMock
            ->expects($this->any())
            ->method('get')
            ->willReturnMap([
                ['Handler\ResourceItem', $resourceHandlerMock],
                [RecipeTransformer::class, $recipeTransformerMock],
            ]);

        $resourceHandlerMock
            ->expects($this->once())
            ->method('setTransformer')
            ->with($this->identicalTo($recipeTransformerMock));

        $this->assertSame(
            $resourceHandlerMock,
            $this->objectUnderTest->__invoke($containerMock, 'some name', [])
        );
    }
}