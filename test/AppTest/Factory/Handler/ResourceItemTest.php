<?php


namespace AppTest\Factory\Handler;

use App\Handler\Resource as ResourceHandler;
use App\Factory\Handler\ResourceItem as ResourceItemFactory;
use Interop\Container\ContainerInterface;
use League\Fractal\Resource\Item as FractalResourceItem;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class NotFoundTest
 * @package AppTest\Factory\Handler
 */
class ResourceItemTest extends TestCase
{
    /** @var ResourceItemFactory $objectUnderTest */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new ResourceItemFactory();
    }

    public function testImplementsFactoryInterface(): void
    {
        $this->assertInstanceOf(
            FactoryInterface::class,
            $this->objectUnderTest
        );
    }

    public function testInvoke(): void
    {
        $containerMock = $this->createMock(ContainerInterface::class);

        $resourceHandlerMock = $this->createMock(ResourceHandler::class);
        $containerMock
            ->expects($this->once())
            ->method('get')
            ->with(ResourceHandler::class)
            ->willReturn($resourceHandlerMock);

        $resourceHandlerMock
            ->expects($this->once())
            ->method('setFractalResource')
            ->with($this->isInstanceOf(FractalResourceItem::class));

        $this->assertSame(
            $resourceHandlerMock,
            $this->objectUnderTest->__invoke($containerMock, 'some name', [])
        );
    }
}