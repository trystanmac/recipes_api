<?php

declare(strict_types=1);

namespace AppTest\Factory\Transformer;

use App\Factory\Transformer\Recipe as RecipeTransformerFactory;
use App\Transformer\Recipe as RecipeTransformer;
use PHPUnit\Framework\TestCase;

class RecipeTest extends TestCase
{
    /** @var RecipeTransformerFactory RecipeTransformerFactory */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new RecipeTransformerFactory();
    }

    public function testInvoke()
    {
        $this->assertInstanceOf(
            RecipeTransformer::class,
            $this->objectUnderTest->__invoke()
        );
    }
}