<?php


namespace AppTest\Factory;

use App\Factory\EntityManager as EntityManagerFactory;
use DateTime;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class EntityManagerTest
 * @package AppTest\Factory
 */
class EntityManagerTest extends TestCase
{
    /** @var EntityManagerFactory EntityManagerFactory */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new EntityManagerFactory();
    }

    public function testImplementsOfZendFactory(): void
    {
        $this->assertInstanceOf(
            FactoryInterface::class,
            $this->objectUnderTest
        );
    }

    public function testInvoke(): void
    {
        $containerMock = $this->createMock(ContainerInterface::class);

        $this->setUpContainerConfig($containerMock);

        $this->assertInstanceOf(
            EntityManager::class,
            $this->objectUnderTest->__invoke($containerMock, 'some name', [])
        );
    }

    public function testDatabaseConnected()
    {
        $containerMock = $this->createMock(ContainerInterface::class);

        $this->setUpContainerConfig($containerMock);

        /** @var EntityManager $entityManager */
        $entityManager = $this->objectUnderTest->__invoke($containerMock, 'some name', []);

        $statement = $entityManager
            ->getConnection()
            ->prepare("SELECT datetime('now') as now");

        $statement->execute();
        $result = $statement->fetchAll();

        $this->assertEquals(
            (new DateTime())->format('Y-m-d H:i:s'),
            $result[0]['now']
        );
    }

    /**
     * @param ContainerInterface $containerMock
     */
    private function setUpContainerConfig(ContainerInterface $containerMock): void
    {
        $config = [
            'doctrine' => [
                'entityPaths' => [
                    sprintf('%s/src/App/src/Entity', getcwd()),
                ],
                'databasePath' => sprintf('%s/data/sqlite.test.db', getcwd()),
                'driver' => 'pdo_sqlite',
            ],
        ];

        $containerMock
            ->expects($this->any())
            ->method('get')
            ->willReturnMap([
                ['config', $config],
            ]);
    }
}