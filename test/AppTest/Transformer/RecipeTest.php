<?php

declare(strict_types=1);

namespace AppTest\Transformer;

use App\Entity\Recipe as RecipeEntity;
use App\Transformer\Recipe as RecipeTransformer;
use League\Fractal\TransformerAbstract;
use PHPUnit\Framework\TestCase;

/**
 * Class RecipeTest
 * @package AppTest\Transformer
 */
class RecipeTest extends TestCase
{
    /** @var RecipeTransformer $objectUnderTest */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new RecipeTransformer();
    }

    public function testIsInstanceOfTransformerAbstract(): void
    {
        $this->assertInstanceOf(
            TransformerAbstract::class,
            $this->objectUnderTest
        );
    }

    public function testTransform(): void
    {
        $recipeEntityMock = $this->createMock(RecipeEntity::class);

        $id = rand(3000, 4000);
        $recipeEntityMock
            ->expects($this->once())
            ->method('getId')
            ->willReturn($id);

        $cuisine = uniqid();
        $recipeEntityMock
            ->expects($this->once())
            ->method('getCuisine')
            ->willReturn($cuisine);

        $slug = uniqid();
        $recipeEntityMock
            ->expects($this->once())
            ->method('getSlug')
            ->willReturn($slug);

        $rating = rand(1000, 2000);
        $recipeEntityMock
            ->expects($this->once())
            ->method('getRating')
            ->willReturn($rating);

        $transformedData = [
            'id' => $id,
            'cuisine' => $cuisine,
            'slug' => $slug,
            'rating' => $rating,
        ];

        $this->assertEquals(
            $transformedData,
            $this->objectUnderTest->transform($recipeEntityMock)
        );
    }
}