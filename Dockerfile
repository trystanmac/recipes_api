FROM php:7.2.1-apache
MAINTAINER egidio docile

RUN  apt-get update
RUN  apt-get install -y zlib1g-dev libicu-dev g++
RUN  docker-php-ext-configure intl
RUN  docker-php-ext-install intl

RUN docker-php-ext-install pdo pdo_mysql mysqli

ADD ./build/000-default.conf /etc/apache2/sites-available/000-default.conf

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php --install-dir=/usr/local/bin
RUN mv /usr/local/bin/composer.phar /usr/local/bin/composer
RUN php -r "unlink('composer-setup.php');"

RUN a2enmod rewrite
RUN service apache2 restart
